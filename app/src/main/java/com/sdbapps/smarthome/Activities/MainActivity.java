package com.sdbapps.smarthome.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sdbapps.smarthome.Source.FirebaseReferences;
import com.sdbapps.smarthome.Model.CircleTransformation;
import com.sdbapps.smarthome.Model.Tabla;
import com.sdbapps.smarthome.R;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private String[] header = {"Date", "Temperature", "Room", "Image"};
    private ArrayList<String[]> rows = new ArrayList<>();
    private Tabla tableLogs;
    private String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ejecutar();
        ImageView r1 = findViewById(R.id.R1cir);
        Glide.with(this)
                .load(R.drawable.ic_r1)
                .error(R.drawable.ic_error)
                .transform(new CircleTransformation(this))
                .override(40, 40)
                .circleCrop()
                .into(r1);

        ImageView r2 = findViewById(R.id.R2cir);
        Glide.with(this)
                .load(R.drawable.ic_r2)
                .error(R.drawable.ic_error)
                .transform(new CircleTransformation(this))
                .override(40, 40)
                .circleCrop()
                .into(r2);

        ImageView r3 = findViewById(R.id.R3cir);
        Glide.with(this)
                .load(R.drawable.ic_r3)
                .error(R.drawable.ic_error)
                .transform(new CircleTransformation(this))
                .override(40, 40)
                .circleCrop()
                .into(r3);

        TableLayout tabl = findViewById(R.id.Logs);
        tableLogs = new Tabla(tabl, getApplicationContext());
        tableLogs.addHeader(header);
        tableLogs.addData(getLogs());

        MaterialButton btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);
            }
        });

        MaterialButton btnGuardar = findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data = tableLogs.takeData();
                generateFile(data);
                }
        });
    }

    public ArrayList<String[]> getLogs() {
        rows.add(new String[]{"", "", "", ""});
        return rows;
    }

    private void ejecutar() {
        final Handler handler1 = new Handler();
        final Handler handler2 = new Handler();
        final Handler handler3 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                setRoom1();
                handler1.postDelayed(this, 15000);
            }
        }, 5000);
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                setRoom2();
                handler2.postDelayed(this, 10000);
            }
        }, 5000);
        handler3.postDelayed(new Runnable() {
            @Override
            public void run() {
                setRoom3();
                handler3.postDelayed(this, 5000);
            }
        }, 5000);
    }

    public void setRoom1() {
        MaterialTextView r = findViewById(R.id.R1temp);
        int t;
        t = (int) (Math.random() * 16) + 5;
        if (t < 12) {
            save(t, "1");
        }
        String temp = String.valueOf(t) + "°";
        r.setText(temp);
        if (t >= 5 && t <= 14) {
            ImageView r1 = findViewById(R.id.R1cir);
            Glide.with(this)
                    .load(R.mipmap.ic_lightb_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r1);
        } else if (t >= 15 && t < 25) {
            ImageView r1 = findViewById(R.id.R1cir);
            Glide.with(this)
                    .load(R.mipmap.ic_yellow_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r1);
        } else if (t >= 25) {
            ImageView r1 = findViewById(R.id.R1cir);
            Glide.with(this)
                    .load(R.mipmap.ic_orange_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r1);
        }
    }

    public void setRoom2() {
        MaterialTextView r = findViewById(R.id.R2temp);
        int t;
        t = new Random().nextInt(18) + 8;
        if (t < 12) {
            save(t, "2");
        }
        String temp = String.valueOf(t) + "°";
        r.setText(temp);
        if (t >= 5 && t <= 14) {
            ImageView r2 = findViewById(R.id.R2cir);
            Glide.with(this)
                    .load(R.mipmap.ic_lightb_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r2);
        } else if (t >= 15 && t < 25) {
            ImageView r2 = findViewById(R.id.R2cir);
            Glide.with(this)
                    .load(R.mipmap.ic_yellow_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r2);
        } else if (t >= 25) {
            ImageView r2 = findViewById(R.id.R2cir);
            Glide.with(this)
                    .load(R.mipmap.ic_orange_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r2);
        }
    }

    public void setRoom3() {
        MaterialTextView r = findViewById(R.id.R3temp);
        int t;
        t = (int) (Math.random() * 19) + 10;
        if (t < 12) {
            save(t, "3");
        }
        String temp = String.valueOf(t) + "°";
        r.setText(temp);
        if (t >= 5 && t <= 14) {
            ImageView r3 = findViewById(R.id.R3cir);
            Glide.with(this)
                    .load(R.mipmap.ic_lightb_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r3);
        } else if (t >= 15 && t < 25) {
            ImageView r3 = findViewById(R.id.R3cir);
            Glide.with(this)
                    .load(R.mipmap.ic_yellow_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r3);
        } else if (t >= 25) {
            ImageView r3 = findViewById(R.id.R3cir);
            Glide.with(this)
                    .load(R.mipmap.ic_orange_round)
                    .error(R.drawable.ic_error)
                    .transform(new CircleTransformation(this))
                    .override(100, 100)
                    .circleCrop()
                    .into(r3);
        }
    }

    public String image;

    public void save(int t, String r) {
        Date fecha = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String f;
        String tem;
        final String[] imagen = new String[1];
        f = sdf.format(fecha);
        String[] fe = f.split(" ");
        tem = String.valueOf(t);

        DatabaseReference descripcionC = FirebaseDatabase.getInstance().getReference(FirebaseReferences.roomsdb).child("room" + r).child("imagen");
        descripcionC.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                image = snapshot.getValue().toString();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        String[] item = new String[]{fe[0] + "\n" + fe[1], tem + "%", r, image};
        tableLogs.addItems(item);
    }

    public void generateFile(String sBody) {
        checkStorage();
        File root = null,gpxfile=null;
        try {root = new File(Environment.getExternalStorageDirectory(), "LogsSmartHome");
            if (!root.exists()) {
                root.mkdirs();
            }
            gpxfile = new File(root, "logs.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(this, "Logs file\n Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String path = gpxfile.getPath();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
        sendIntent.setType("text/rtf");
        startActivity(Intent.createChooser(sendIntent,getResources().getText(R.string.sendTo)));

    }

    public void checkStorage(){
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i("Message", "Permission denied.");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 225);
        } else {
            Log.i("Message", "Permission granted.");
        }
    }
}