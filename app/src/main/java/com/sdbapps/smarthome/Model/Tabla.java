package com.sdbapps.smarthome.Model;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.sdbapps.smarthome.Activities.MainActivity;
import com.sdbapps.smarthome.R;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Tabla {
    private TableLayout tableLayout;
    private Context context;
    private ArrayList<String[]>data;
    private String[] header;
    private TableRow tableRow;
    private TextView txtCell;
    private int indexC, indexR;

    public Tabla(TableLayout tableLayout, Context context){
        this.tableLayout=tableLayout;
        this.context=context;
    }

    public void addHeader(String[]header){
        this.header=header;
        createHeader();
    }
    public void addData(ArrayList<String[]>data){
        this.data=data;
        createDataTable();
    }
    private void newRow(){
        tableRow=new TableRow(context);
    }
    private void newCell(){
        txtCell = new TextView(context);
        txtCell.setGravity(Gravity.CENTER);
        txtCell.setTextSize(15);
    }
    private void createHeader(){
        indexC=0;
        newRow();
        while (indexC<header.length){
            newCell();
            txtCell.setText(header[indexC++]);
            tableRow.addView(txtCell,newTableRowParams());
        }
        tableLayout.addView(tableRow);
    }
    private void createDataTable(){
        String info;
        for(indexR=1;indexR<=data.size();indexR++){
            newRow();
            for (indexC=0; indexC < data.size(); indexC++){
                newCell();
                String[] row = data.get(indexR-1);
                info = (indexC < row.length) ? row[indexC] : "";
                txtCell.setText(info);
                tableRow.addView(txtCell,newTableRowParams());
            }
            tableLayout.addView(tableRow);
        }
    }

    private TableRow.LayoutParams newTableRowParams(){
        TableRow.LayoutParams params = new TableRow.LayoutParams();
        params.setMargins(1,1,1,1);
        params.weight=1;
        return params;
    }

    public void addItems(String[] item){
        String info="";
        data.add(item);
        indexC=0;
        newRow();
        if(data.size()<=31){
            while (indexC<header.length){
                newCell();
                if(indexC==3){
                    if(item[2].equals("1")){
                        txtCell.setCompoundDrawablesWithIntrinsicBounds(0,0,R.mipmap.ic_room1,0);
                    }else if (item[2].equals("2")){
                        txtCell.setCompoundDrawablesWithIntrinsicBounds(0,0,R.mipmap.ic_room2,0);
                    }else if (item[2].equals("3")){
                        txtCell.setCompoundDrawablesWithIntrinsicBounds(0,0,R.mipmap.ic_room3,0);
                    }
                    indexC++;
                }else {
                    info = (indexC < item.length) ? item[indexC++] : "";
                    txtCell.setText(info);
                }
                tableRow.addView(txtCell,newTableRowParams());
            }
            tableLayout.addView(tableRow,data.size()-1);

        }else{
            Toast.makeText(context,"Logs limit data: 30 \n No more data will be enter.",Toast.LENGTH_SHORT);

        }
    }

    public String takeData(){
        String r="Date,Temperature,Room\n",r1="";
        String[] row=null;
        if(data.size()<=1){
            Toast.makeText(context,"No Data to share",Toast.LENGTH_SHORT);
        }else {
            for (indexR = 2; indexR <= data.size(); indexR++) {
                for (indexC = 0; indexC < data.size() - 1; indexC++) {
                    row = data.get(indexR - 1);
                    r1 = (row[0].replace("\n", " ") + "," + row[1] + "," + row[2] + "\n");
                }
                r+=r1;
            }
        }
        return r;
    }


}
